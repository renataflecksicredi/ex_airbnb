package com.sicredi.exairbnb.service;

import com.sicredi.exairbnb.DTO.ImovelDTO;
import com.sicredi.exairbnb.DTO.UsuarioDTO;
import com.sicredi.exairbnb.DTO.UsuarioImovelDTO;
import com.sicredi.exairbnb.entity.Usuario;
import com.sicredi.exairbnb.repository.UsuarioRepository;
import com.sicredi.exairbnb.service.Exceptions.DatabaseException;
import com.sicredi.exairbnb.service.Exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private ImovelService imovelService;

    @Transactional(readOnly = true)
    public Page<UsuarioDTO> findAll(Pageable pageable){
        Page<Usuario> lista = repository.findAll(pageable);
        return lista.map(UsuarioDTO::new);
}

    @Transactional(readOnly = true)
    public UsuarioImovelDTO findByIdWithRealty(String id) {
        UsuarioDTO UsuarioDTO = this.findById(id);
        String imovelId = UsuarioDTO.getImovel();
        ImovelDTO imovelDTO = imovelService.findById(imovelId);
        return new UsuarioImovelDTO(UsuarioDTO, imovelDTO);
    }

    @Transactional(readOnly = true)
    public UsuarioDTO findById(String id) {
        Optional<Usuario> obj = repository.findById(id);
        Usuario entity = obj.orElseThrow(() -> new ResourceNotFoundException("Usuarios não encontrado"));
        return new UsuarioDTO(entity);
    }

    @Transactional
    public UsuarioDTO insert(UsuarioDTO dto) {
        Usuario entity = new Usuario();
        copyDTOToEntity(dto, entity);
        entity = repository.save(entity);
        return new UsuarioDTO(entity);
    }

    private void copyDTOToEntity(UsuarioDTO dto, Usuario entity) {
        entity.setName(dto.getName());
        entity.setUser_name(dto.getUser_name());
        entity.setPassword(dto.getPassword());
        entity.setUser_type(dto.getUser_type());
        entity.setImovel(dto.getImovel());
    }

    @Transactional
    public UsuarioDTO update(String id, UsuarioDTO dto) {
        try {
            Usuario entity = repository.getReferenceById(id);
            copyDTOToEntity(dto, entity);
            entity = repository.save(entity);
            return new UsuarioDTO(entity);
        }
        catch(EntityNotFoundException e) {
            throw new ResourceNotFoundException("Id" + id + "not found");
        }
    }

    public void delete (String id) {
        try {
            repository.deleteById(id);
            } catch (EmptyResultDataAccessException e) {
                throw new ResourceNotFoundException("Usuário não encontrado");
            } catch (DataIntegrityViolationException e) {
                throw new DatabaseException("Violação de integridade");
            }

    }
}