package com.sicredi.exairbnb.service;

import com.sicredi.exairbnb.DTO.ImovelDTO;
import com.sicredi.exairbnb.entity.Imovel;
import com.sicredi.exairbnb.repository.ImovelRepository;
import com.sicredi.exairbnb.service.Exceptions.DatabaseException;
import com.sicredi.exairbnb.service.Exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Optional;

@Service
public class ImovelService implements Serializable {
    @Autowired
    private ImovelRepository imovelRepository;

    @Transactional(readOnly = true)
    public Page<ImovelDTO> findAll(Pageable pageable){
        Page<Imovel> list = imovelRepository.findAll(pageable);
        return list.map(ImovelDTO::new);
    }

    @Transactional(readOnly = true)
    public ImovelDTO findById(String id){
        Optional<Imovel> optionalImovel = imovelRepository.findById(id);
        Imovel imovel = optionalImovel.orElseThrow(() -> new ResourceNotFoundException("Entity not found"));
        return new ImovelDTO(imovel);
    }

    @Transactional
    public ImovelDTO insert(ImovelDTO imovelDTO) {
        Imovel entity = new Imovel();
        copyDtoToEntity(imovelDTO, entity);
        entity = imovelRepository.save(entity);
        return new ImovelDTO(entity);
    }

    @Transactional
    public ImovelDTO update(String id, ImovelDTO imovelDTO) {
        try{
            Imovel entity = imovelRepository.getReferenceById(id);
            copyDtoToEntity(imovelDTO, entity);
            entity = imovelRepository.save(entity);
            return new ImovelDTO(entity);
        }catch (ResourceNotFoundException e){
            throw new ResourceNotFoundException("Id not found " + id);
        }
    }
    public void delete(String id) {
        try {
            imovelRepository.deleteById(id);
        }catch (EmptyResultDataAccessException e){
            throw new ResourceNotFoundException("Id not found " + id);
        }catch (DataIntegrityViolationException e){
            throw new DatabaseException("Integrity violation");
        }
    }
    @Transactional
    private void copyDtoToEntity(ImovelDTO imoveldto, Imovel entity) {
        entity.setNome(imoveldto.getNome());
        entity.setCidade(imoveldto.getCidade());
        entity.setTipo(imoveldto.getTipo());
        entity.setPrecoDiario(imoveldto.getPrecoDiario());
    }    
}