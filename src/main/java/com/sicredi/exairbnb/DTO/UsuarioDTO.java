package com.sicredi.exairbnb.DTO;

import java.io.Serializable;

import com.sicredi.exairbnb.entity.Usuario;

import com.sicredi.exairbnb.util.TipoUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String user_name;
    private String password;
    private TipoUser user_type;

    private String imovel;

    public UsuarioDTO(Usuario Usuario) {
        this.id = Usuario.getId();
        this.name = Usuario.getName();
        this.user_name = Usuario.getUser_name();
        this.password = Usuario.getPassword();
        this.user_type = Usuario.getUser_type();
        this.imovel = Usuario.getImovel();
    }
}
