package com.sicredi.exairbnb.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UsuarioImovelDTO {
    private UsuarioDTO user;
    private ImovelDTO realty;

    public UsuarioImovelDTO(UsuarioDTO usuario, ImovelDTO imovel) {
        this.user = usuario;
        this.realty = imovel;
    }
}
