package com.sicredi.exairbnb.DTO;

import com.sicredi.exairbnb.entity.Imovel;
import com.sicredi.exairbnb.util.TipoImovel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImovelDTO implements Serializable {
    private String id;
    private String nome;
    private TipoImovel tipo;
    private String cidade;
    private Double precoDiario;

    public ImovelDTO(Imovel entity) {
        this.id = entity.getId();
        this.nome = entity.getNome();
        this.tipo = entity.getTipo();
        this.cidade = entity.getCidade();
        this.precoDiario = entity.getPrecoDiario();
    }
}