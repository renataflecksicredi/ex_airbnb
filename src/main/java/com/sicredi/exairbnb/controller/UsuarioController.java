package com.sicredi.exairbnb.controller;

import com.sicredi.exairbnb.DTO.UsuarioDTO;
import com.sicredi.exairbnb.DTO.UsuarioImovelDTO;
import com.sicredi.exairbnb.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
@RestController
@RequestMapping (value = "/usuarios")
public class UsuarioController {
    @Autowired
    private UsuarioService service;

    @GetMapping
    public ResponseEntity<Page<UsuarioDTO>> findAll(Pageable pageable){
        Page<UsuarioDTO> obj = service.findAll(pageable);
        return ResponseEntity.ok().body(obj);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<UsuarioDTO> findById(@PathVariable String id) {
        UsuarioDTO dto = service.findById(id);
        return ResponseEntity.ok().body(dto);
    }

    @GetMapping(value = "/imoveis/{id}")
    public ResponseEntity<UsuarioImovelDTO> findByIdWithRealty(@PathVariable String id) {
        UsuarioImovelDTO usuarioImovelDTO = service.findByIdWithRealty(id);
        return ResponseEntity.ok().body(usuarioImovelDTO);
    }

    @PostMapping
    public ResponseEntity<UsuarioDTO> insert(@RequestBody UsuarioDTO dto) {
        dto = service.insert(dto);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/id}")
                .buildAndExpand(dto.getId()).toUri();

        return ResponseEntity.created(uri).body(dto);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UsuarioDTO> update (@PathVariable String id, @RequestBody UsuarioDTO dto) {
        dto = service.update(id, dto);
        return ResponseEntity.ok().body(dto);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
