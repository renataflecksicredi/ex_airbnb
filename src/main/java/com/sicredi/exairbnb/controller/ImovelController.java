package com.sicredi.exairbnb.controller;

import com.sicredi.exairbnb.DTO.ImovelDTO;
import com.sicredi.exairbnb.service.ImovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/imoveis")
public class ImovelController {
    @Autowired
    private ImovelService imovelService;

    @GetMapping()
    public ResponseEntity<Page<ImovelDTO>> findAll(Pageable pageable) {
        Page<ImovelDTO> list = imovelService.findAll(pageable);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ImovelDTO> findById(@PathVariable String id) {
        ImovelDTO imovelDTO = imovelService.findById(id);
        return ResponseEntity.ok().body(imovelDTO);
    }

    @PostMapping
    public ResponseEntity<ImovelDTO> insert(@RequestBody ImovelDTO imoveldto) {
        imoveldto = imovelService.insert(imoveldto);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(imoveldto.getId())
                .toUri();
        return ResponseEntity.created(uri).body(imoveldto);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<ImovelDTO> update(@PathVariable String id, @RequestBody ImovelDTO imoveldto) {
        imoveldto = imovelService.update(id, imoveldto);
        return ResponseEntity.ok().body(imoveldto);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ImovelDTO> delete(@PathVariable String id) {
        imovelService.delete(id);
        return ResponseEntity.noContent().build();
    }
}