package com.sicredi.exairbnb.util;

public enum TipoUser {
    LOCADOR, LOCATARIO;
}
