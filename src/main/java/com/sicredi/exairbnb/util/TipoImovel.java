package com.sicredi.exairbnb.util;

public enum TipoImovel {
    APARTAMENTO, CASA,  QUARTO;
}