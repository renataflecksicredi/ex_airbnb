package com.sicredi.exairbnb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class ExairbnbApplication {
	public static void main(String[] args) {
		SpringApplication.run(ExairbnbApplication.class, args);
	}
}