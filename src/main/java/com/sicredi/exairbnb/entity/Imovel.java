package com.sicredi.exairbnb.entity;

import com.sicredi.exairbnb.DTO.ImovelDTO;
import com.sicredi.exairbnb.util.TipoImovel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;


import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "imoveis")
public class Imovel implements Serializable {
    @Id
    private String id;
    private String nome;
    private TipoImovel tipo;
    private String cidade;
    private Double precoDiario;

    public Imovel(ImovelDTO imovelDTO) {
        this.id = imovelDTO.getId();
        this.nome = imovelDTO.getNome();
        this.tipo = imovelDTO.getTipo();
        this.cidade = imovelDTO.getCidade();
        this.precoDiario = imovelDTO.getPrecoDiario();
    }
}