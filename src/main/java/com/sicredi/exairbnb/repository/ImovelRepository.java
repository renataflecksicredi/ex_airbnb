package com.sicredi.exairbnb.repository;

import com.sicredi.exairbnb.entity.Imovel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImovelRepository extends MongoRepository<Imovel, Long> {
    Imovel getReferenceById(String id);

    Optional<Imovel> findById(String id);

    void deleteById(String id);
}

