package com.sicredi.exairbnb.repository;


import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.sicredi.exairbnb.entity.Usuario;

@Repository
public interface UsuarioRepository extends MongoRepository<Usuario, Long>{

    Usuario getReferenceById(Long id);
    Optional<Usuario> findById(String id);

    Usuario getReferenceById(String id);

    void deleteById(String id);
}
